---
# Language Configuration.
language:

margin:

# Invoice Configuration.
title:
number:
currency:
date:
deadline:

receiver:
receiver-address:
  -

name:
address:
  -
phone:
email:
iban:
bic:

items:
  - name:
    vat:
    price:
    total:

subtotal:
vat:
total:
---
