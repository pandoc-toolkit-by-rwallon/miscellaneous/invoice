\documentclass{article}
\usepackage[margin=$if(margin)$$margin$$else$1cm$endif$]{geometry}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[$if(language)$$language$$else$english$endif$]{babel}

\usepackage{eurosym}
\usepackage[$if(language)$$language$$else$english$endif$]{simpleinvoice}
\pagenumbering{gobble}

\newcommand{\currency}{$if(currency)$$currency$$else$\euro$endif$}

\begin{document}
\setinvoicetitle{$title$}
$if(number)$\setinvoicenumber{$number$}$endif$
\setinvoicedate{$if(date)$$date$$else$\today$endif$}
$if(deadline)$\setdeadline{$deadline$}$endif$

\setreceivername{$receiver$}
\setreceiveraddress{$for(receiver-address)$$receiver-address$$sep$\\$endfor$}

\setname{$name$}
\setaddress{$for(address)$$address$$sep$\\$endfor$}{$for(address)$$address$$sep$\\$endfor$}
$if(phone)$\setphonenumber{$phone$}$endif$
$if(email)$\setemail{$email$}$endif$
\setaccountnumber{IBAN: $iban$ BIC: $bic$}

$for(items)$\additem{$items.name$}{$items.price$ \currency}{$items.vat$}{$items.total$ \currency}$endfor$

\setsubtotal{$if(subtotal)$$subtotal$$else$$total$$endif$ \currency}
$if(vat)$\setvat{$vat$ \currency}$endif$
\settotal{$total$ \currency}
\makeinvoice
\end{document}